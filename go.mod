module gitlab.com/gitlab-org/security-products/analyzers/common/v3

require (
	github.com/bmatcuk/doublestar v1.3.4
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.8.0
	github.com/urfave/cli/v2 v2.3.0
	golang.org/x/sys v0.0.0-20200915084602-288bc346aa39 // indirect
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b // indirect
)

go 1.13
