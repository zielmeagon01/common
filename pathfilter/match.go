package pathfilter

import (
	"path/filepath"
	"strings"

	"github.com/bmatcuk/doublestar"
)

const separator = string(filepath.Separator)

// Match reports whether path matches the pattern.
//
// It is based on filepath.Match but also shares some behaviors of gitignore:
//
// - File/directory name match. If the pattern contains no slash then split the path
// in parts and call filepath.Match on each part.
//
// - Parent directory match. Right-truncate the given path to have as many parts
// as pattern has and call filepath.March on the truncated path
//
// Unlike gitignore it ignores a trailing slash character
// and does not distinguish directories from regular files.
// Leading slash is ignored.
//
func Match(pattern, path string) (bool, error) {
	isAbsPattern := strings.HasPrefix(pattern, separator)
	isGlobstarPattern := strings.Contains(pattern, "**")

	// trim leading slash, detect leading slash in pattern
	pattern = strings.TrimPrefix(pattern, separator)
	path = strings.TrimPrefix(path, separator)

	// split in parts
	patternParts := strings.Split(pattern, separator)
	pathParts := strings.Split(path, separator)

	switch {
	case len(patternParts) == 1 && !isAbsPattern:
		// iterate on parts of path
		for _, p := range pathParts {
			matched, err := filepath.Match(pattern, p)
			if err != nil || matched {
				return matched, err
			}
		}
		return false, nil

	// don't truncate globstar patterns as they might unintentionally match
	// certain paths, e.g. the pattern `a/**/b` will match the path `a/c`
	// if truncated
	case len(pathParts) > len(patternParts) && !isGlobstarPattern:
		// truncate path to match pattern
		var truncated = filepath.Join(pathParts[0:len(patternParts)]...)
		return filepath.Match(pattern, truncated)

	default:
		// fall back to doublestar.Match
		return doublestar.Match(pattern, path)
	}
}
